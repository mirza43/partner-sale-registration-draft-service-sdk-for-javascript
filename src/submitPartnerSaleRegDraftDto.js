/**
 * @class {SubmitPartnerSaleRegDraftDto}
 */
export default class SubmitPartnerSaleRegDraftDto{

    _id:number;

    _submittedByName:string;

    _partnerRepId:string;

    _firstName:string;

    _lastName:string;

    _emailAddress:string;

    /**
     * @param {number} id
     * @param {string} submittedByName
     * @param {string} partnerRepId
     * @param {string} firstName
     * @param {string} lastName
     * @param {string} emailAddress
     */
    constructor(
        id:number,
        submittedByName:string,
        partnerRepId:string,
        firstName:string,
        lastName:string,
        emailAddress:string
    ){

        if(!id){
            throw new TypeError('id required');
        }
        this._id = id;

        if(!submittedByName){
            throw new TypeError('submittedByName required');
        }
        this._submittedByName = submittedByName;

        this._partnerRepId = partnerRepId;

        this._firstName = firstName;

        this._lastName = lastName;

        this._emailAddress = emailAddress;
    }

    /**
     * getter methods
     */

    get id():number{
        return this._id;
    }

    get submittedByName():string{
        return this._submittedByName;
    }

    get partnerRepId():string{
        return this._partnerRepId;
    }

    get firstName():string{
        return this._firstName;
    }

    get lastName():string{
        return this._lastName;
    }

    get emailAddress():string{
        return this._emailAddress;
    }

    toJSON() {
        return {
            id: this._id,
            submittedByName: this._submittedByName,
            partnerRepId: this._partnerRepId,
            firstName: this._firstName,
            lastName: this._lastName,
            emailAddress: this._emailAddress
        }
    }

}


