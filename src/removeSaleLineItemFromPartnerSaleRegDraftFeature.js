import {inject} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import PartnerSaleRegistrationDraftServiceSdkConfig from './partnerSaleRegistrationDraftServiceSdkConfig';

@inject(PartnerSaleRegistrationDraftServiceSdkConfig, HttpClient)
class RemoveSaleLineItemFromPartnerSaleRegDraftFeature {

    _config:PartnerSaleRegistrationDraftServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:PartnerSaleRegistrationDraftServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * @param partnerSaleRegDraftSaleLineItemId
     * @param accessToken
     * @returns {string}
     */
    execute(partnerSaleRegDraftSaleLineItemId:number,
            accessToken:string):Promise<number> {

        return this._httpClient
            .createRequest(`partner-sale-registration/removesalelineitem/${partnerSaleRegDraftSaleLineItemId}`)
            .asDelete()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .then(response => response.content);

    }
}

export default RemoveSaleLineItemFromPartnerSaleRegDraftFeature;
