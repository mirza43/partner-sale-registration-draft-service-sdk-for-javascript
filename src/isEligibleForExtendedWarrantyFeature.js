import {inject} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import ExtendedWarrantyRequestRuleWebView from './extendedWarrantyRequestRuleWebView';
import PartnerSaleRegistrationDraftServiceSdkConfig from './partnerSaleRegistrationDraftServiceSdkConfig';

@inject(PartnerSaleRegistrationDraftServiceSdkConfig, HttpClient)
class IsEligibleForExtendedWarrantyFeature {

    _config:PartnerSaleRegistrationDraftServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:PartnerSaleRegistrationDraftServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     *
     * @param {ExtendedWarrantyRequestRuleWebView} request
     * @param {string} accessToken
     * @returns {Promise.<boolean>}
     */
    execute(request:ExtendedWarrantyRequestRuleWebView,
            accessToken:string):Promise<boolean> {

        return this._httpClient
            .createRequest('partner-sale-registration/isextendedwarrantyeligible')
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .then(response => response.content);
    }
}

export default IsEligibleForExtendedWarrantyFeature;