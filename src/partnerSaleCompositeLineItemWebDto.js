import PartnerSaleSimpleLineItemWebDto from './partnerSaleSimpleLineItemWebDto';
/**
 * @class {PartnerSaleCompositeLineItemWebDto}
 */
export default class PartnerSaleCompositeLineItemWebDto{

    _components:PartnerSaleSimpleLineItemWebDto[];

    constructor(
        components:PartnerSaleSimpleLineItemWebDto[]
    ){

        this._components = components;

    }

    /**
     * getter methods
     */
    get components():PartnerSaleSimpleLineItemWebDto[]{
        return this._components;
    }

    toJSON() {
        return {
            components: this._components
        }
    }

}


