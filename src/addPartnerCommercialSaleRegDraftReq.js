import PartnerSaleRegDraftSaleSimpleLineItem from './partnerSaleRegDraftSaleSimpleLineItem';
import PartnerSaleRegDraftSaleCompositeLineItem from './partnerSaleRegDraftSaleCompositeLineItem';

/**
 * @class {AddPartnerCommercialSaleRegDraftReq}
 */
export default class AddPartnerCommercialSaleRegDraftReq{

    _facilityContactId:string;

    _facilityId:string;

    _facilityName:string;

    _partnerAccountId:string;

    _sellDate:string;

    _installDate:string;

    _invoiceNumber:string;

    _customerBrandName:string;

    _customerSourceId:number;

    _managementCompanyId:string;

    _buyingGroupId:string;

    _partnerRepUserId:string;

    _invoiceUrl:string;

    _isSubmitted:boolean;

    _simpleLineItems:PartnerSaleRegDraftSaleSimpleLineItem[] ;

    _compositeLineItems:PartnerSaleRegDraftSaleCompositeLineItem[];

    constructor(
        facilityContactId:string,
        facilityId:string,
        facilityName:string,
        partnerAccountId:string,
        sellDate:string,
        installDate:string,
        invoiceNumber:string,
        customerBrandName:string,
        customerSourceId:number,
        managementCompanyId:string,
        buyingGroupId:string,
        partnerRepUserId:string,
        invoiceUrl:string,
        isSubmitted:boolean,
        simpleLineItems:PartnerSaleRegDraftSaleSimpleLineItem[],
        compositeLineItems:PartnerSaleRegDraftSaleCompositeLineItem[]
    ){

        if(!facilityContactId){
            throw new TypeError('facilityContactId required');
        }
        this._facilityContactId = facilityContactId;

        if(!facilityId){
            throw new TypeError('facilityId required');
        }
        this._facilityId = facilityId;

        if(!facilityName){
            throw new TypeError('facilityName required');
        }
        this._facilityName = facilityName;

        if(!partnerAccountId){
            throw new TypeError('partnerAccountId required');
        }
        this._partnerAccountId = partnerAccountId;

        if(!sellDate){
            throw new TypeError('sellDate required');
        }
        this._sellDate = sellDate;

        if(!installDate){
            throw new TypeError('installDate required');
        }
        this._installDate = installDate;

        if(!invoiceNumber){
            throw new TypeError('invoiceNumber required');
        }
        this._invoiceNumber = invoiceNumber;

        this._customerBrandName = customerBrandName;

        this._customerSourceId = customerSourceId;

        this._managementCompanyId = managementCompanyId;

        this._buyingGroupId = buyingGroupId;

        this._partnerRepUserId = partnerRepUserId;

        this._invoiceUrl = invoiceUrl;

        this._isSubmitted = isSubmitted;

        this._simpleLineItems = simpleLineItems;

        this._compositeLineItems = compositeLineItems;

    }

    /**
     * getter methods
     */
    get facilityContactId():string{
        return this._facilityContactId;
    }

    get facilityId():string{
        return this._facilityId;
    }

    get facilityName():string{
        return this._facilityName;
    }

    get partnerAccountId():string{
        return this._partnerAccountId;
    }

    get sellDate():string{
        return this._sellDate;
    }

    get installDate():string{
        return this._installDate;
    }

    get invoiceNumber():string{
        return this._invoiceNumber;
    }

    get customerBrandName():string{
        return this._customerBrandName;
    }

    get customerSourceId():number{
        return this._customerSourceId;
    }

    get managementCompanyId():string{
        return this._managementCompanyId;
    }

    get buyingGroupId():string{
        return this._buyingGroupId;
    }

    get partnerRepUserId():string{
        return this._partnerRepUserId;
    }

    get invoiceUrl():string{
        return this._invoiceUrl;
    }

    get isSubmitted():boolean{
        return this._isSubmitted;
    }

    get simpleLineItems():PartnerSaleRegDraftSaleSimpleLineItem[]{
        return this._simpleLineItems;
    }

    get compositeLineItems():PartnerSaleRegDraftSaleCompositeLineItem[]{
        return this._compositeLineItems;
    }

    toJSON() {
        return {
            facilityContactId: this._facilityContactId,
            facilityId: this._facilityId,
            facilityName: this._facilityName,
            partnerAccountId: this._partnerAccountId,
            sellDate: this._sellDate,
            installDate: this._installDate,
            invoiceNumber: this._invoiceNumber,
            customerBrandName:this._customerBrandName,
            customerSourceId: this._customerSourceId,
            managementCompanyId: this._managementCompanyId,
            buyingGroupId: this._buyingGroupId,
            partnerRepUserId: this._partnerRepUserId,
            invoiceUrl: this._invoiceUrl,
            isSubmitted: this._isSubmitted,
            simpleLineItems: this._simpleLineItems,
            compositeLineItems: this._compositeLineItems
        }
    }

}

