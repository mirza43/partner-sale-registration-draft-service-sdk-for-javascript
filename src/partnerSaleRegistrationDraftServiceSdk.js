import PartnerSaleRegistrationDraftServiceSdkConfig from './partnerSaleRegistrationDraftServiceSdkConfig';
import DiContainer from './diContainer';
import AddPartnerCommercialSaleRegDraftReq from './addPartnerCommercialSaleRegDraftReq';
import PartnerCommercialSaleRegDraftView from './partnerCommercialSaleRegDraftView';
import AddPartnerCommercialSaleRegDraftFeature from './addPartnerCommercialSaleRegDraftFeature';
import PartnerCommercialSaleRegSynopsisView from './partnerCommercialSaleRegSynopsisView';
import GetPartnerCommercialSaleRegDraftWithIdFeature from './getPartnerCommercialSaleRegDraftWithIdFeature';
import GetPartnerCommercialSaleRegDraftWithAccountIdFeature from './getPartnerCommercialSaleRegDraftWithAccountIdFeature';
import SubmitPartnerCommercialSaleRegDraftFeature from './submitPartnerCommercialSaleRegDraftFeature';
import UpdatePartnerCommercialSaleRegDraftReq from './updatePartnerCommercialSaleRegDraftReq';
import UpdatePartnerCommercialSaleRegDraftFeature from './updatePartnerCommercialSaleRegDraftFeature';
import DeletePartnerSaleRegDraftWithIdFeature from './deletePartnerSaleRegDraftWithIdFeature';
import UpdateSaleInvoiceUrlOfPartnerSaleRegDraftReq from './updateSaleInvoiceUrlOfPartnerSaleRegDraftReq';
import ProductSaleRegistrationRuleEngineWebDto from './productSaleRegistrationRuleEngineWebDto';
import ExtendedWarrantyRequestRuleWebView from './extendedWarrantyRequestRuleWebView';
import SubmitPartnerSaleRegDraftDto from './submitPartnerSaleRegDraftDto';
import UpdateSaleInvoiceUrlOfPartnerSaleRegDraftFeature from './updateSaleInvoiceUrlOfPartnerSaleRegDraftFeature';
import RemoveSaleLineItemFromPartnerSaleRegDraftFeature from './removeSaleLineItemFromPartnerSaleRegDraftFeature';
import IsEligibleForSpiffFeature from './isEligibleForSpiffFeature';
import IsEligibleForExtendedWarrantyFeature from './isEligibleForExtendedWarrantyFeature';
import GetPartnerSaleLineItemsExcludeSerialNumberWithDraftId from './getPartnerSaleLineItemsExcludeSerialNumberWithDraftId';

/**
 * @class {PartnerSaleRegistrationServiceSdk}
 */
export default class PartnerSaleRegistrationServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {PartnerSaleRegistrationDraftServiceSdkConfig} config
     */
    constructor(config:PartnerSaleRegistrationDraftServiceSdkConfig) {

        this._diContainer = new DiContainer(config);

    }

    /**
     *
     * @param request
     * @param accessToken
     * @returns {Promise.<number>}
     */
    addPartnerCommercialSaleRegDraft(
        request:AddPartnerCommercialSaleRegDraftReq,
        accessToken:string):Promise<number>{

        return this
            ._diContainer
            .get(AddPartnerCommercialSaleRegDraftFeature)
            .execute(
                request,
                accessToken
            );

    }

    /**
     *
     * @param partnerSaleRegDraftId
     * @param accessToken
     * @returns {Promise.<PartnerCommercialSaleRegSynopsisView>}
     */
    getPartnerSaleRegistrationDraftWithDraftId(
        partnerSaleRegDraftId:number,
        accessToken:string):Promise<PartnerCommercialSaleRegSynopsisView> {

        return this
            ._diContainer
            .get(GetPartnerCommercialSaleRegDraftWithIdFeature)
            .execute(
                partnerSaleRegDraftId,
                accessToken
            );

    }

    /**
     *
     * @param {string} partnerAccountId
     * @param {string} accessToken
     * @returns {Promise.<PartnerCommercialSaleRegSynopsisView[]>}
     */
    getPartnerCommercialSaleRegDraftWithAccountId(
        partnerAccountId:string,
        accessToken:string):Promise<PartnerCommercialSaleRegSynopsisView[]>{

        return this
            ._diContainer
            .get(GetPartnerCommercialSaleRegDraftWithAccountIdFeature)
            .execute(
                partnerAccountId,
                accessToken
            );

    }

    /**
     *
     * @param {SubmitPartnerSaleRegDraftDto} request
     * @param {string} accessToken
     * @returns {Promise.<number>|Promise.<PartnerCommercialSaleRegSynopsisView>}
     */
    submitPartnerCommercialSaleRegDraft(
        request:SubmitPartnerSaleRegDraftDto,
        accessToken:string):Promise<PartnerCommercialSaleRegDraftView>{

        return this
            ._diContainer
            .get(SubmitPartnerCommercialSaleRegDraftFeature)
            .execute(
                request,
                accessToken
            );

    }

    /**
     *
     * @param {UpdatePartnerCommercialSaleRegDraftReq} request
     * @param {number} partnerSaleRegDraftId
     * @param {string} accessToken
     * @returns {PartnerCommercialSaleRegSynopsisView}
     */
    updatePartnerCommercialSaleRegDraft(request:UpdatePartnerCommercialSaleRegDraftReq,
                                        partnerSaleRegDraftId:number,
                                        accessToken:string):Promise<PartnerCommercialSaleRegSynopsisView>{
        return this
            ._diContainer
            .get(UpdatePartnerCommercialSaleRegDraftFeature)
            .execute(
                request,
                partnerSaleRegDraftId,
                accessToken
            );

    }

    /**
     * @param {number} partnerSaleRegDraftId
     * @param {string} accessToken
     * @returns {*}
     */
    deletePartnerSaleRegDraftWithId(partnerSaleRegDraftId:number,accessToken:string){

        return this
            ._diContainer
            .get(DeletePartnerSaleRegDraftWithIdFeature)
            .execute(
                partnerSaleRegDraftId,
                accessToken
            );

    }

    /**
     * @param {UpdateSaleInvoiceUrlOfPartnerSaleRegDraftReq} request
     * @param {string} accessToken
     * @returns {*}
     */
    updateSaleInvoiceUrlOfPartnerSaleRegDraft(request:UpdateSaleInvoiceUrlOfPartnerSaleRegDraftReq,
                                              accessToken:string){

        return this
            ._diContainer
            .get(UpdateSaleInvoiceUrlOfPartnerSaleRegDraftFeature)
            .execute(
                request,
                accessToken
            );

    }

    /**
     * @param {number} partnerSaleRegDraftSaleLineItemId
     * @param {string} accessToken
     * @returns {string}
     */
    removeSaleLineItemFromPartnerSaleRegDraft(partnerSaleRegDraftSaleLineItemId:number,
                                              accessToken:string):Promise<string>{

        return this
            ._diContainer
            .get(RemoveSaleLineItemFromPartnerSaleRegDraftFeature)
            .execute(
                partnerSaleRegDraftSaleLineItemId,
                accessToken
            );

    }

    /**
     * @param {ProductSaleRegistrationRuleEngineWebDto} request
     * @param {string} accessToken
     * @returns {boolean}
     */
    isEligibleForSpiff(request:ProductSaleRegistrationRuleEngineWebDto,
                       accessToken:string):Promise<boolean>{

        return this
                ._diContainer
                .get(IsEligibleForSpiffFeature)
                .execute(
                    request,
                    accessToken
                );

    }

    /**
     * @param {ExtendedWarrantyRequestRuleWebView} request
     * @param {string} accessToken
     * @returns {boolean}
     */
    isEligibleForExtendedWarranty(request:ExtendedWarrantyRequestRuleWebView,
                       accessToken:string):Promise<boolean>{

        return this
            ._diContainer
            .get(IsEligibleForExtendedWarrantyFeature)
            .execute(
                request,
                accessToken
            );

    }

    /**
     *
     * @param partnerSaleRegDraftId
     * @param accessToken
     * @returns {Promise.<PartnerCommercialSaleRegSynopsisView>}
     */
    getPartnerSaleLineItemsExcludeSerialNumberWithDraftId(
        partnerSaleRegDraftId:number,
        accessToken:string):Promise<PartnerCommercialSaleRegSynopsisView> {

        return this
            ._diContainer
            .get(GetPartnerSaleLineItemsExcludeSerialNumberWithDraftId)
            .execute(
                partnerSaleRegDraftId,
                accessToken
            );

    }

}
