import config from './config';
import jwt from 'jwt-simple';
import dummy from '../dummy';
import PartnerSaleRegDraftSaleSimpleLineItem from '../../src/partnerSaleRegDraftSaleSimpleLineItem';
import PartnerSaleRegDraftSaleCompositeLineItem from '../../src/partnerSaleRegDraftSaleCompositeLineItem';
import PartnerSaleLineItemComponent from '../../src/partnerSaleLineItemComponent';

export default {
    constructValidAppAccessToken,
    constructValidPartnerRepOAuth2AccessToken,
    constructSimpleLineItems,
    constructCompositeLineItems
}


function constructValidAppAccessToken():string {

    const tenMinutesInMilliseconds = 10000 * 60;

    const jwtPayload = {
        "type": "app",
        "exp": Date.now() + tenMinutesInMilliseconds,
        "aud": dummy.url,
        "iss": dummy.url
    };

    return jwt.encode(jwtPayload, config.identityServiceJwtSigningKey);
}

function constructValidPartnerRepOAuth2AccessToken():string {

    const tenMinutesInMilliseconds = 10000 * 60;

    const jwtPayload = {
        "type": 'partnerRep',
        "exp": Date.now() + tenMinutesInMilliseconds,
        "aud": dummy.url,
        "iss": dummy.url,
        "given_name": dummy.firstName,
        "family_name": dummy.lastName,
        "sub": dummy.url,
        "account_id": dummy.partnerAccountId,
        "sap_vendor_number": dummy.sap_vendor_number
    };

    return jwt.encode(jwtPayload, config.identityServiceJwtSigningKey);
}

function constructSimpleLineItems():PartnerSaleRegDraftSaleSimpleLineItem[]{

    return [{
        assetId:dummy.assetId,
        serialNumber:dummy.serialNumber,
        productLineId:dummy.productLineId,
        price:dummy.price,
        productLineName:dummy.productLineName,
        productGroupId:dummy.productGroupId,
        productGroupName:dummy.productGroupName
        }]
    ;
}

function constructSaleLineItemComponents():PartnerSaleLineItemComponent[]{
    return [{
        serialNumber:dummy.serialNumber,
        productLineId:dummy.productLineId,
        productLineName:dummy.productLineName,
        productGroupId:dummy.productGroupId,
        productGroupName:dummy.productGroupName,
        assetId:dummy.assetId,
        price:null
    }];
}

function constructCompositeLineItems():PartnerSaleRegDraftSaleCompositeLineItem[]{
    return [{
        components:constructSaleLineItemComponents(),
        price:dummy.price
    }];
}

