/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
export default  {
    id:109,
    firstName: 'firstName',
    lastName: 'lastName',
    facilityContactId:"123456781234567899",
    facilityId:'123456781234567899',
    facilityName:"facilityName",
    partnerAccountId:"001K000001H2Km2IAF",
    sellDate:'11/20/2015',
    installDate:'11/24/2015',
    invoiceNumber:"1234567",
    customerBrandName:"name",
    customerSourceId:22,
    managementCompanyId:"21",
    buyingGroupId:"25",
    partnerRepUserId:'00u5fj21jtj63phiA0h7',
    invoiceUrl:"url",
    isSubmitted:false,
    url: 'https://dummy-url.com',
    simpleLineItemId:2,
    assetId:"123",
    serialNumber:"1234567",
    productLineId:340,
    price:10,
    productLineName:"line1",
    productGroupId:300,
    productGroupName:"group1",
    submittedByName:"Test User",
    personEmail: 'test@test.com'
};



